const fs = require('fs')

const Router = require('./Router')
const Logger = require('./Logger')

const LOG_FILE_PATH = './logs.json'
const FILES_DIR_PATH = './'

const router = new Router()
const logger = new Logger( LOG_FILE_PATH )

router.post('/file/', ( req, res ) => {
    const filename = req.urlQueryParams.get('filename')
    const content = req.urlQueryParams.get('content')

    if( !filename || !content ) {
        res.statusCode = 400
        res.end('Invalid URL. \n'+
        'The request should contain query string with parameters "filename" and "content" that\n' + 
        'both are non-zero length values.')

        return;
    }


    fs.writeFile( FILES_DIR_PATH + filename, content, error => {
        if( error ) {
            res.statusCode = 500

            res.end('Error has occured on writing a file.')
        }

        res.statusCode = 200

        res.end('File has been successfully written.')

        logger.write( `New file with name '${ filename }' saved`)
    })
})

router.get('/file/:filename', ( req, res ) => {
    const { filename } = req.urlPathParams
    
    fs.readFile( FILES_DIR_PATH + filename, 'utf8', ( error, data ) => {
        if( error ) {
            res.statusCode = 400

            res.end( `Cannot find file with name "${filename}".\n` )
        } else {
            res.statusCode = 200

            res.end( data )
        }
    })

    logger.write( `GET file '${ filename }'`)
})

router.get('/logs/', ( req, res ) => {
    const from = Number.parseInt( req.urlQueryParams.get('from') )
    const to = Number.parseInt( req.urlQueryParams.get('to') )

    fs.readFile( LOG_FILE_PATH, ( error, data ) => {
        if( error ) {
            res.statusCode = 400
            res.end('Error has occured on attempt to read a file.')

            return;
        }

        const logsData = JSON.parse( data )

        if( req.urlQueryParams.has('from') && req.urlQueryParams.has('to') ) {
            logsData.logs = logsData.logs.filter( el => el.time >= from && el.time <= to )
        }

        res.statusCode = 200
        res.end( JSON.stringify( logsData, null, 2 ) )
    })
})

module.exports = router