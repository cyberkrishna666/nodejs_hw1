const Url = require('./Url')
const url = require('url')

class RequestUrl extends Url {
    constructor( path ) {
        super( url.parse( path ).pathname )
        this.path = path
        this.params = {}
        this.hasQueryParams = false
        this.queryParams = null

        this.detectQueryParams()
    }

    detectQueryParams() {
        const queryParams = new URLSearchParams( this.path.split('?')[1] )
        
        if( queryParams ) {
            this.hasQueryParams = true
            this.queryParams = queryParams
        }
    }

    detectRequestParams( endpointUrl ) {
        endpointUrl.pathArray.forEach( ( el, index ) => {
            if( el.param ) {
                console.log('el.value:', el.value);
                this.params[el.value] = this.pathArray[index].value 
            }
        })
    }

    processPath = () => {
        const splittedPath = new Array()

        this.fullPath.length - 1 ? this.fullPath.split('/').forEach( el => {
            if( el ) {
                splittedPath.push( el )
            }
        }) : splittedPath.push( this.fullPath )

        const pathArray = splittedPath.map( el => {
            const result = {
                value: el
            }

            return result
        })

        return pathArray
    }
}

module.exports = RequestUrl