class Url {
    constructor( path ) {
        this.fullPath = path
        this.hasParams = false
        this.pathArray = this.processPath()
    }

    processPath = () => {
        const splittedPath = new Array()

        this.fullPath.length - 1 ? this.fullPath.split('/').forEach( el => {
            if( el ) {
                splittedPath.push( el )
            }
        }) : splittedPath.push( this.fullPath )

        const pathArray = splittedPath.map( el => {
            const result = {
                value: el,
                param: false
            }

            if( el[0] === ':' ) {
                result.param = true
                result.value = el.slice(1)
                !this.hasParams && ( this.hasParams = true )
            }

            return result
        })

        return pathArray
    }
}

module.exports = Url
