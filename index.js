const http = require('http')
const router = require('./routes')

module.exports = () => {
    const port = process.env.PORT || 8080
    const server = http.createServer( ( req, res ) => {
        router.handleRequest( req, res )
    })

    server.listen( port, () => {
        console.log( 'Server started ad port', port )
    })
}
