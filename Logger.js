const fs = require('fs').promises

class Logger {
    constructor( logFilePath ) {
        this.logFilePath = logFilePath
        this.logsTemplate = {
            logs: []
        }
    }

    write( msg ) {
        const msgData = {
            message: msg,
            time: Date.now()
        }
    
        fs.readFile( this.logFilePath )
            .catch( error => {
                if( error.code === 'ENOENT' ) {
                    return null
                } else {
                    console.log('Error on reading file', this.logFilePath, '\nMessage:\n', error );
                }
            })
            .then( fileContent => {
                if( fileContent ) {
                    const fileData = JSON.parse( fileContent )
                    fileData.logs.push( msgData )

                    fs.writeFile(this.logFilePath, `${ JSON.stringify( fileData, null, 2 ) }`)

                } else {
                    const firstLog = { ...this.logsTemplate }
                    firstLog.logs.push( msgData )

                    fs.writeFile(this.logFilePath, `${ JSON.stringify( firstLog, null, 2 ) }`)
                }
            })
    }
}

module.exports = Logger
