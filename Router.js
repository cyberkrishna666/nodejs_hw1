const Endpoint = require('./Endpoint')
const url = require('url')
const RequestUrl = require('./RequestUrl')

class Router {
    constructor() {
        this.endpoints = {
            get: new Array(),
            post: new Array(),
            put: new Array(),
            delete: new Array()
        }

        this.handleRequest = this.handleRequest.bind( this )
    }

    /**
     * @param {string} path
     * @param {function} cb
     * 
    */
    get( path, cb ) {
        this.endpoints.get.push( new Endpoint( path, cb ) )
    }

    /**
     * @param {string} path
     * @param {function} cb
     * 
    */
    post( path, cb ) {
        this.endpoints.post.push( new Endpoint( path, cb ) )
    }

    put( path, cb ) {
        this.endpoints.put.push( new Endpoint( path, cb ) )
    }

    del( path, cb ) {
        this.endpoints.delete.push( new Endpoint( path, cb ) )
    }

    handleRequest( req, res ) {
        const { method } = req
        const requestUrl = new RequestUrl( req.url )
        let endpoint = null


        const filteredByPathLength = this.endpoints[method.toLowerCase()].filter( el => { 
            return el.url.pathArray.length === requestUrl.pathArray.length
        })

        filteredByPathLength.some( el => {
            let matches = false

            for( let i = 0; i < el.url.pathArray.length; i++ ) {
                if( requestUrl.pathArray[i].value === el.url.pathArray[i].value || el.url.pathArray[i].param ) {
                    matches = true
                } else {
                    matches = false

                    break;
                }
            }

            if( matches ) {
                endpoint = el
            }

            return matches
        })

        if( endpoint ) {
            if( endpoint.url.hasParams ) {
                requestUrl.detectRequestParams( endpoint.url )

                Object.defineProperty(req, 'urlPathParams', {
                    value: requestUrl.params,
                    writable: false
                })
            }

            if( requestUrl.hasQueryParams ) {
                Object.defineProperty(req, 'urlQueryParams', {
                    value: requestUrl.queryParams,
                    writable: false
                })
            }

            endpoint.cb( req, res )
        } else {
            res.statusCode = 404
            res.end('Endpoint not found!')
        }
    }
}

module.exports = Router