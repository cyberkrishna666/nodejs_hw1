const Url = require('./Url')

class Endpoint {
    constructor( path, cb ) {
        this.cb = cb
        this.url = new Url( path )
    }
}

module.exports = Endpoint
